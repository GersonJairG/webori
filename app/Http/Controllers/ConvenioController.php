<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Convenio;
use App\Models\Institucion;

class ConvenioController extends Controller
{
    //TODO: Listo ya funcionan los endpoints, mañana hacer la parte de extraer los datos
    //por medio de una consulta para lo del mapa, y hacer la parte administrativa de convenios y instituciones
    /**
     * Regresa el listado de convenios
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Convenio::all();
        $result = Convenio::join('tipo_convenio', 'convenio.id_tipo_convenio', '=', 'tipo_convenio.id')
                ->join('institucion', 'convenio.id_institucion', '=', 'institucion.id')            
                ->select('convenio.*', 'tipo_convenio.nombre as tipo_convenio','institucion.nombre as institucion')
                ->get();
        return $result;
    }

    public function count(){
        // $var = Convenio::all();        
        $result = Convenio::selectRaw('pais.nombre, count(*) convenios')
                        ->join('institucion', 'convenio.id_institucion', '=', 'institucion.id')            
                        ->join('pais', 'institucion.id_pais', '=' , 'pais.id')
                        ->groupBy('pais.nombre')                        
                        ->get();
        return $result;
    }

    /**
     * Registra una nuevo convenio
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $convenio = new Convenio;
        $convenio->año= $request['año'];
        // $convenio->id_pais= $request['idPais'];
        $convenio->id_tipo_convenio= $request['idTipoConvenio'];
        $convenio->id_institucion= $request['idInstitucion'];
        
        $convenio->save();  
        
        return $convenio;
    }
    /**
     * Consulta un convenio dado su id y retorna la información de este
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Convenio::find($id);
    }

    public function showForCountry($country){
        $result = Convenio::join('institucion', 'convenio.id_institucion', '=', 'institucion.id')
                        ->join('tipo_convenio', 'convenio.id_tipo_convenio', '=', 'tipo_convenio.id')
                        ->join('pais', 'institucion.id_pais', '=', 'pais.id')
                        ->where('pais.nombre','=',$country)
                        ->select('convenio.año as año', 'institucion.nombre as institucion','tipo_convenio.nombre as tipo_convenio')
                        ->get();      
                        
        return $result;
                        
    }

    

    /**
     * Actualiza la información de un convenio dado su id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $convenio = Convenio::find($id);  
        $convenio->año = isset($request['año']) ? $request['año'] : $convenio->año; 
        // $convenio->id_pais = isset($request['idPais']) ? $request['idPais'] : $convenio->id_pais; 
        $convenio->id_tipo_convenio = isset($request['idTipoConvenio']) ? $request['idTipoConvenio'] : $convenio->id_tipo_convenio; 
        $convenio->id_institucion = isset($request['idInstitucion']) ? $request['idInstitucion'] : $convenio->id_institucion; 
        $convenio->save();        
        return $convenio;
    }

    /**
     * Elimina el registro de un convenio dado su id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $convenio = Convenio::find($id);
        $id = $convenio->id;
        $convenio->delete();

        return "Convenio #'{$id}' eliminado";
    }
}
