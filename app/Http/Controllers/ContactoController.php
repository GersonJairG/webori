<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contacto;

class ContactoController extends Controller
{
    /**
     * Regresa el listado de contactos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Contacto::all();
    }

    /**
     * Registra una nuevo contacto
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contacto = new Contacto;
        $contacto->ubicacion_u= $request['ubicacionU'];
        $contacto->ubicacion_ori= $request['ubicacionOri'];
        $contacto->email= $request['email'];
        $contacto->telefono= $request['telefono'];
        $contacto->facebook= $request['facebook'];
        $contacto->google= $request['google'];    
        $contacto->twitter= $request['twitter'];
        $contacto->linkedIn= $request['linkedIn'];
        $contacto->linkedIn= $request['youtube'];
        $contacto->linkedIn= $request['instagram'];
        $contacto->save();        
        return $contacto;
    }
    /**
     * Consulta un contacto dado su id y retorna la información de este
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Contacto::find($id);
    }

    /**
     * Actualiza la información de un contacto dado su id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contacto = Contacto::find($id);  
        $contacto->ubicacion_u = isset($request['ubicacionU']) ? $request['ubicacionU'] : $contacto->ubicacion_u; 
        $contacto->ubicacion_ori = isset($request['ubicacionOri']) ? $request['ubicacionOri'] : $contacto->ubicacion_ori; 
        $contacto->email = isset($request['email']) ? $request['email'] : $contacto->email;
        $contacto->telefono = isset($request['telefono']) ? $request['telefono'] : $contacto->telefono;
        $contacto->facebook = isset($request['facebook']) ? $request['facebook'] : $contacto->facebook;
        $contacto->twitter = isset($request['twitter']) ? $request['twitter'] : $contacto->twitter;
        $contacto->google = isset($request['google']) ? $request['google'] : $contacto->google;
        $contacto->linkedIn = isset($request['linkedIn']) ? $request['linkedIn'] : $contacto->linkedIn;
        $contacto->youtube = isset($request['youtube']) ? $request['youtube'] : $contacto->youtube;
        $contacto->instagram = isset($request['instagram']) ? $request['instagram'] : $contacto->instagram;
        $contacto->save();        
        return $contacto;
    }

    /**
     * Elimina el registro de un contacto dado su id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contacto = Contacto::find($id);        
        $contacto->delete();

        return "contacto eliminado";
    }
}
