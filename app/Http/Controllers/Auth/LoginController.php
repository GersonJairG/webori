<?php

namespace App\Http\Controllers\Auth;


use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){                 
        if (Auth::attempt([
            'email' => $request['correo'],
            'password' => $request['contrasena']
        ])){         
                // Authentication passed...
            $datos = array(
                "id" => Auth::user()->id,
                "nombre" => Auth::user()->nombre,
                "documento" => Auth::user()->documento,
                "email" => Auth::user()->email,            
                "sexo" => Auth::user()->sexo,
                "rol" => Auth::user()->id_rol                
            ); 
            if($datos["rol"]!=1){
                return $datos;
            }                          
        }
        return [];        
    }
}


