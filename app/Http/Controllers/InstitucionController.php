<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Institucion;

class InstitucionController extends Controller
{
    /**
     * Regresa el listado de instituciones
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Institucion::all();
        $result = Institucion::join('pais', 'institucion.id_pais', '=', 'pais.id')
                ->join('tipo_institucion', 'institucion.id_tipo_institucion', '=', 'tipo_institucion.id')
                // ->select('institucion.id', 'institucion.nombre', 'pais.nombre as pais','tipo_institucion.nombre as tipo')
                ->select('institucion.*', 'pais.nombre as pais','tipo_institucion.nombre as tipo')
                ->get();
        return $result;
    }

    /**
     * Registra una nuevo institucion
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $institucion = new Institucion;
        $institucion->nombre= $request['nombre'];
        $institucion->id_pais= $request['idPais'];
        $institucion->id_tipo_institucion= $request['idTipoInstitucion'];
        $institucion->save();                

        return $institucion;
    }
    /**
     * Consulta un institucion dado su id y retorna la información de este
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Institucion::find($id);
    }

    /**
     * Actualiza la información de un instituciones dado su id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $institucion = Institucion::find($id);  
        $institucion->nombre = isset($request['nombre']) ? $request['nombre'] : $institucion->nombre; 
        $institucion->id_pais = isset($request['idPais']) ? $request['idPais'] : $institucion->id_pais;         
        $institucion->id_tipo_institucion = isset($request['idTipoInstitucion']) ? $request['idTipoInstitucion'] : $institucion->id_tipo_institucion;         
        $institucion->save();        
        return $institucion;
    }

    /**
     * Elimina el registro de un convenio dado su id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $institucion = Institucion::find($id);
        $nombre = $institucion->nombre;
        $institucion->delete();

        return "Institucion '{$nombre}' eliminada";
    }
}