<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Regresa el listado de usuarios
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAll()
    {
        return User::all();
    }

    /**
     * Regresa el listado de usuarios
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return User::join('rol','usuario.id_rol','=','rol.id')
                    ->select('usuario.*','rol.nombre as rol')
                    ->get();
    }
    
    /**
     * Registra un nuevo usuario
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = new User;
        $usuario->id= $request['id'];
        $usuario->nombre= $request['nombre'];
        $usuario->documento= $request['documento'];
        $usuario->email= $request['correo'];        
        $usuario->password= bcrypt($request['contrasena']);
        // $usuario->contrasena= Hash::make($request['contrasena']);
        $usuario->sexo= $request['sexo'];
        $usuario->id_rol= $request['idRol'];
        $usuario->save();        
        return $usuario;
    }
    /**
     * Consulta un usuario dado su id y retorna la información de este
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::find($id);
    }

    /**
     * Actualiza la información de un usuario dado su id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $usuario = User::find($request['id']);
        //si exite el name del request lo pone, si no pone el nombre de la actividad.
        $usuario->nombre = isset($request['nombre']) ? $request['nombre'] : $usuario->nombre; 
        $usuario->documento = isset($request['documento']) ? $request['documento'] : $usuario->documento;
        $usuario->email= isset($request['correo']) ? $request['correo'] : $usuario->email;                   
        // $usuario->contrasena= isset($request['contrasena']) ? $request['contrasena'] : $usuario->contrasena;
        $usuario->sexo= isset($request['sexo']) ? $request['sexo'] : $usuario->sexo; 
        $usuario->id_rol= isset($request['idRol']) ? $request['idRol'] : $usuario->id_rol;
        $usuario->save();        
        return $usuario;
    }

    /**
     * Elimina el registro de un usuario dado su id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::find($id);
        $nombre = $usuario->nombre;
        $usuario->delete();

        return "Usuario '{$nombre}' eliminado";
    }
}
