<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Experiencia;

class ExperienciaController extends Controller
{
    /**
     * Regresa el listado de experiencias
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Experiencia::all();
    }

    /**
     * Registra una nueva Experiencia
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $experiencia = new Experiencia;
        $experiencia->titulo= $request['titulo'];
        $experiencia->descripcion= $request['descripcion'];    
        $experiencia->save();        
        return $experiencia;
    }
    /**
     * Consulta una Experiencia dado su id y retorna la información de este
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Experiencia::find($id);
    }

    /**
     * Actualiza la información de una Experiencia dado su id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $experiencia = Experiencia::find($id);  
        $experiencia->titulo = isset($request['titulo']) ? $request['titulo'] : $experiencia->titulo; 
        $experiencia->descripcion = isset($request['descripcion']) ? $request['descripcion'] : $experiencia->descripcion;
        $experiencia->save();        
        return $experiencia;
    }

    /**
     * Elimina el registro de una Experiencia dado su id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $experiencia = Experiencia::find($id);
        $titulo = $experiencia->titulo;
        $experiencia->delete();

        return "Experiencia '{$titulo}' eliminada";
    }
}
