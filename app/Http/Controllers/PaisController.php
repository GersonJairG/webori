<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pais;

class PaisController extends Controller
{
    /**
     * Regresa el listado de paises
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Pais::all();
    }
    
}