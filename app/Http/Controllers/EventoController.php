<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Evento;

class EventoController extends Controller
{
    /**
     * Regresa el listado de eventos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Evento::all();
    }

    /**
     * Registra una nuevo evento
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $evento = new Evento;
        $evento->nombre= $request['nombre'];
        $evento->descripcion= $request['descripcion'];    
        $evento->lugar= $request['lugar'];  
        $evento->fecha= $request['fecha'];
        $evento->save();        
        return $evento;
    }
    /**
     * Consulta un evento dado su id y retorna la información de este
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Evento::find($id);
    }

    /**
     * Actualiza la información de un evento dado su id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $evento = Evento::find($id);  
        $evento->nombre = isset($request['nombre']) ? $request['nombre'] : $evento->nombre; 
        $evento->descripcion = isset($request['descripcion']) ? $request['descripcion'] : $evento->descripcion;
        $evento->lugar = isset($request['lugar']) ? $request['lugar'] : $evento->lugar;
        $evento->fecha = isset($request['fecha']) ? $request['fecha'] : $evento->fecha;
        $evento->save();        
        return $evento;
    }

    /**
     * Elimina el registro de un evento dado su id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $evento = Evento::find($id);
        $nombre = $evento->nombre;
        $evento->delete();

        return "Evento '{$nombre}' eliminado";
    }
}
