<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $nombre
 * @property string $descripcion
 */
class Convenio extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'convenio';

    /**
     * @var array
     */
    protected $fillable = ['año', 'fecha', 'id_tipo_convenio', 'id_institucion'];

    //belongsTo de tipo_convenio
    public function tiposConvenio(){
        return $this->belongsTo('App\Models\TipoConvenio');
    }

    //belongsTo de pais
    public function instituciones(){
        return $this->belongsTo('App\Models\Institucion');
    }    




}