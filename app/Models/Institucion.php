<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $nombre
 * @property string $descripcion
 */
class Institucion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'institucion';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'id_pais'];

    //relacion uno a muchos con convenios
    public function convenios(){
        return $this->hasMany('App\Models\Convenio', 'id_institucion');
    }
    
     //belongsTo de pais
     public function paises(){
        return $this->belongsTo('App\Models\Pais');
    } 

    //belongsTo de tipo_instituciones
    public function tiposInstituciones(){
        return $this->belongsTo('App\Models\TipoInstitucion');
    }   
    
}