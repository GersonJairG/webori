<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $nombre
 * @property string $descripcion
 */
class Contacto extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'contacto';

    /**
     * @var array
     */
    protected $fillable = ['ubicacion_u','ubicacion_ori', 'email', 'telefono','facebook','google','twitter','linkedIn','youtube','instagram'];

}
