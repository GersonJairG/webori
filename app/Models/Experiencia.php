<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 */
class Experiencia extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'experiencia';

    /**
     * @var array
     */
    protected $fillable = ['titulo','descripcion'];

}
