<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $nombre
 * @property string $descripcion
 */
class Pais extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'pais';

    /**
     * @var array
     */
    protected $fillable = ['nombre'];

    //relacion uno a muchos con convenio
    public function convenios(){
        return $this->hasMany('App\Models\Institucion', 'id_pais');
    }

}



