<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $nombre
 * @property string $descripcion
 */
class TipoInstitucion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipo_institucion';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion'];

    //relacion uno a muchos con convenios
    public function intituciones(){
        return $this->hasMany('App\Models\Convenio', 'id_tipo_institucion');
    }

}
