<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS, PATCH, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin");


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Al probar en postman, recordar que los nombres de las variables seran tal cual como se las pasamos en el Controlador.
// TODO: Averiguar bien si en actualizaciones (PATCH) se deben enviar los datos a cambiar por params.
//actividades
Route::prefix('experiencias')->group(function () {
    Route::get('/', 'ExperienciaController@index');//listo
    Route::post('/new', 'ExperienciaController@store');//listo(Parametros en body)
    Route::get('/{id}', 'ExperienciaController@show');//listo
    Route::patch('/{id}', 'ExperienciaController@update');//listo(id escrito en link, y parametros en params)
    Route::delete('/{id}', 'ExperienciaController@destroy');//listo
});

//contratos
Route::prefix('eventos')->group(function () {
    Route::get('/', 'EventoController@index');//listo
    Route::post('/new', 'EventoController@store');//listo(Parametros en body)
    Route::get('/{id}', 'EventoController@show');//listo
    Route::patch('/{id}', 'EventoController@update');//listo(id escrito en link, y parametros en params)
    Route::delete('/{id}', 'EventoController@destroy');//listo
});

//usuarios
Route::prefix('usuarios')->group(function () {
    Route::get('/', 'UserController@index');//listo
    Route::post('/new', 'UserController@store');//listo(Parametros en body)
    Route::get('/{id}', 'UserController@show');//listo
    Route::patch('/{id}', 'UserController@update');//listo(id escrito en link, y parametros en params)
    Route::delete('/{id}', 'UserController@destroy');//listo    
});

//contactos
Route::prefix('contactos')->group(function () {
    Route::get('/', 'ContactoController@index');//listo
    Route::post('/new', 'ContactoController@store');//listo(Parametros en body)
    Route::get('/{id}', 'ContactoController@show');//listo
    Route::patch('/{id}', 'ContactoController@update');//listo(id escrito en link, y parametros en params)
    Route::delete('/{id}', 'ContactoController@destroy');//listo    
});

//convenios
Route::prefix('convenios')->group(function () {
    Route::get('/', 'ConvenioController@index');//listo
    Route::post('/new', 'ConvenioController@store');//listo(Parametros en body)
    Route::get('/{id}', 'ConvenioController@show');//listo
    Route::patch('/{id}', 'ConvenioController@update');//listo(id escrito en link, y parametros en params)
    Route::delete('/{id}', 'ConvenioController@destroy');//listo    
    //para vista public
    Route::get('/count', 'ConvenioController@count');//listo
});

//convenios vista publica
Route::prefix('convenios-mapa')->group(function () {    
    Route::get('/count', 'ConvenioController@count');//listo
    Route::get('/{country}', 'ConvenioController@showForCountry');//listo
    
});

//instituciones
Route::prefix('instituciones')->group(function () {
    Route::get('/', 'InstitucionController@index');//listo
    Route::post('/new', 'InstitucionController@store');//listo(Parametros en body)
    Route::get('/{id}', 'InstitucionController@show');//listo
    Route::patch('/{id}', 'InstitucionController@update');//listo(id escrito en link, y parametros en params)
    Route::delete('/{id}', 'InstitucionController@destroy');//listo    
});

//paises
Route::prefix('paises')->group(function () {
    Route::get('/', 'PaisController@index');//listo
    // Route::post('/new', 'PaisController@store');//listo(Parametros en body)
    // Route::get('/{id}', 'PaisController@show');//listo
    // Route::patch('/{id}', 'PaisController@update');//listo(id escrito en link, y parametros en params)
    // Route::delete('/{id}', 'PaisController@destroy');//listo    
});


Route::post('/login','Auth\LoginController@login');