<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolTableSeeder::class);
        $this->call(PaisTableSeeder::class);
        $this->call(UsuarioTableSeeder::class);
        $this->call(ExperienciaTableSeeder::class);
        $this->call(EventoTableSeeder::class);
        $this->call(ContactoTableSeeder::class);
        $this->call(TipoConvenioTableSeeder::class);
        $this->call(TipoInstitucionTableSeeder::class);
        $this->call(InstitucionTableSeeder::class);
        $this->call(ConvenioTableSeeder::class);
        
        

    }
}
    //Semillas de la tabla contrato

class UsuarioTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('usuario')->delete();

        $usuarios =
        [
            //usuario de prueba: 1151124, 1090500632, secret
            ['id' => '1151124' , 'nombre' => 'Gerson Guerrero' , 'documento' => '1090500632', 'email' => 'admin@admin.com', 'password' => '$2y$10$K4KhMF6Vg7eJYJK5rFASauxs12Cmql3SvS6FJtsvBSmUNLRsD/VZe','sexo' => 'Masculino', 'id_rol' => 3 ]
        ];

        foreach ($usuarios as $usuario) {
            DB::table('usuario')->insert($usuario);
        }

    }

}

class RolTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('rol')->delete();

        $roles =
        [
            ['id' => 1 , 'nombre' => 'Estudiante' , 'descripcion' => 'Tendra acceso a visualizar...'],
            ['id' => 2 , 'nombre' => 'Docente' , 'descripcion' => 'Tendra acceso a visualizar...'],            
            ['id' => 3 , 'nombre' => 'Admin' , 'descripcion' => 'Tendra acceso a editar...']
        ];

        foreach ($roles as $rol) {
            DB::table('rol')->insert($rol);
        }

    }

}

class TipoInstitucionTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('tipo_institucion')->delete();

        $tipos =
        [
            [ 'id' => 1 , 'nombre' => 'INSTITUCIONES DE EDUCACION SUPERIOR EXTRANJERAS', 'descripcion' => 'descripcion'],
            [ 'id' => 2 , 'nombre' => 'INSTITUCIONES EXTRANJERAS', 'descripcion' => 'descripcion'],
            
        ];

        foreach ($tipos as $tipo) {
            DB::table('tipo_institucion')->insert($tipo);
        }

    }

}

class ExperienciaTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('experiencia')->delete();

        $experiencias =
        [
            ['id' => 1 , 'titulo' => 'William Armesto, estudiante del programa de Derecho UFPS en la Universidad Nacional del Sur.' , 'descripcion' => 'William Armesto, estudiante del programa de Derecho quien actualmente cursa su semestre de movilidad en la Universidad Nacional del Sur UNS gracias a la beca otorgada en el marco del Programa de Movilidad Académica Colombia – Argentina MACA; nos comparte fotografías de su experiencia académica y cultural en Argentina.']            
        ];

        foreach ($experiencias as $experiencia) {
            DB::table('experiencia')->insert($experiencia);
        }

    }

}

class EventoTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('evento')->delete();

        $eventos =
        [
            ['id' => 1 , 'nombre' => 'Año nuevo' , 'descripcion' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit, libero porttitor lacinia mattis curabitur venenatis varius imperdiet, maecenas integer magnis tincidunt gravida viverra. Eros porta vulputate vestibulum phasellus etiam habitant placerat class euismod, tempor sociis urna in penatibus ut posuere hendrerit proin venenatis, cursus sapien malesuada iaculis sollicitudin dictum ac condimentum. Hendrerit interdum vitae tincidunt volutpat tellus bibendum parturient inceptos eleifend eget nulla dictumst, neque lacus commodo ligula semper integer quisque cursus potenti nam.', 'lugar' => 'Hogar', 'fecha' =>'2018-12-31 00:00:00']            
        ];

        foreach ($eventos as $evento) {
            DB::table('evento')->insert($evento);
        }

    }

}

class ContactoTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('contacto')->delete();

        $contactos =
        [
            ['id' => '1', 'ubicacion_u' => 'Avenida Gran Colombia No. 12E-96 / Barrio Colsag, Cúcuta, Colombia' , 'ubicacion_ori' => 'Edificio Torre Administrativa, Of. TA 304' , 'email' => 'relacionesinternacionales@ufps.edu.co', 'telefono' => ' (57) 7 5752664 - 5776655 Ext. 240', 'facebook' => 'https://www.facebook.com/Ufps.edu.co/','twitter' => 'https://twitter.com/UFPSCUCUTA', 'google' => '', 'linkedIn' => '', 'instagram' => 'https://www.instagram.com/ufpscucuta/' , 'youtube' => 'https://www.youtube.com/channel/UCgPz-qqaAk4lbHfr0XH3k2g' ]
        ];

        foreach ($contactos as $contacto) {
            DB::table('contacto')->insert($contacto);
        }

    }

}

class PaisTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('pais')->delete();

        $paises =
        [
            ['nombre'=>'Germany'],
            ['nombre'=>'United States'],
            ['nombre'=>'Brazil'],
            ['nombre'=>'Canada'],
            ['nombre'=>'France'],          
            ['nombre'=>'Russia'],
            ['nombre'=>'Colombia'],
            ['nombre'=>'Afghanistan'],      
            ['nombre'=>'Argentina'],
            ['nombre'=>'Australia'],
            ['nombre'=>'Mexico'],
            ['nombre'=>'Cuba'],
            ['nombre'=>'Chile'],
            ['nombre'=>'Austria'],
            ['nombre'=>'Paraguay'],
            ['nombre'=>'Uruguay'],
            ['nombre'=>'Ecuador'],
            ['nombre'=>'Bolivia'],
            ['nombre'=>'Guyana'],   
            ['nombre'=>'Suriname'],
            ['nombre'=>'French Guiana'],
            ['nombre'=>'Venezuela'],
            ['nombre'=>'Peru'],
            ['nombre'=>'Panama'],
            ['nombre'=>'Costa Rica'],  
            ['nombre'=>'Nicaragua'],
            ['nombre'=>'Honduras'],
            ['nombre'=>'El salvador'],
            ['nombre'=>'Guatemala'],  
            ['nombre'=>'Belize'],
            ['nombre'=>'Jamaica'],
            ['nombre'=>'Haiti'],    
            ['nombre'=>'Dominican Republic'],
            ['nombre'=>'Puerto Rico'],
            ['nombre'=>'Greenland'],
            ['nombre'=>'Iceland'],
            ['nombre'=>'Mongolia'],
            ['nombre'=>'China'],
            ['nombre'=>'North Korea'],
            ['nombre'=>'South Korea'],
            ['nombre'=>'Japan'],
            ['nombre'=>'Taiwan'],
            ['nombre'=>'Philippines'],
            ['nombre'=>'Indonesia'],
            ['nombre'=>'Papua New Guinea'],
            ['nombre'=>'Singapore'],
            ['nombre'=>'Malaysia'],
            ['nombre'=>'Vietnam'],
            ['nombre'=>'Thailand'],
            ['nombre'=>'Cambodia'],
            ['nombre'=>'Laos'],    
            ['nombre'=>'Vietnam'],
            ['nombre'=>'Myanmar'],
            ['nombre'=>'Bangladesh'],
            ['nombre'=>'India'],    
            ['nombre'=>'Pakistan'],
            ['nombre'=>'Nepal'],
            ['nombre'=>'Bhutan'],
            ['nombre'=>'Iran'],    
            ['nombre'=>'Iraq'],
            ['nombre'=>'Saudi Arabia' ],
            ['nombre'=>'United Arab Emirates' ],
            ['nombre'=>'Kyrgyzstan' ],    
            ['nombre'=>'Tajikistan' ],
            ['nombre'=>'Uzbekistan' ],
            ['nombre'=>'Kazakhstan' ],
            ['nombre'=>'Turkmenistan' ],    
            ['nombre'=>'Georgia' ],
            ['nombre'=>'Azerbaijan' ],
            ['nombre'=>'Armenia' ],
            ['nombre'=>'Turkey' ],    
            ['nombre'=>'Syria' ],
            ['nombre'=>'Cyprus' ],
            ['nombre'=>'Lebanon' ],     
            ['nombre'=>'Jordan' ],    
            ['nombre'=>'Israel' ],
            ['nombre'=>'Egypt' ],
            ['nombre'=>'Greece' ],
            ['nombre'=>'Bulgaria' ],    
            ['nombre'=>'Romania' ],
            ['nombre'=>'Albania' ],
            ['nombre'=>'Macedonia' ],
            ['nombre'=>'Montenegro' ],    
            ['nombre'=>'Serbia' ],
            ['nombre'=>'Bosnia and Herzegovina' ],
            ['nombre'=>'Croatia' ],
            ['nombre'=>'Hungary' ],    
            ['nombre'=>'Slovenia' ],
            ['nombre'=>'Slovakia' ],
            ['nombre'=>'Italy' ],
            ['nombre'=>'Spain' ],
            ['nombre'=>'Portugal' ],    
            ['nombre'=>'Poland' ],
            ['nombre'=>'Belgium' ],
            ['nombre'=>'Netherlands' ],
            ['nombre'=>'United Kingdom' ],
            ['nombre'=>'Ireland' ],
            ['nombre'=>'Denmark' ],
            ['nombre'=>'Norway' ],
            ['nombre'=>'Sweden' ],
            ['nombre'=>'Finland' ],
            ['nombre'=>'Estonia' ],
            ['nombre'=>'Latvia' ],
            ['nombre'=>'Lithuania' ],
            ['nombre'=>'Belarus' ],
            ['nombre'=>'Ukraine' ],
            ['nombre'=>'Moldova' ],
            ['nombre'=>'Libya' ],
            ['nombre'=>'Algeria' ],
            ['nombre'=>'Tunisia' ],
            ['nombre'=>'Morocco' ],
            ['nombre'=>'Mauritania' ],
            ['nombre'=>'Mali' ],
            ['nombre'=>'Senegal' ],  
            ['nombre'=>'Guinea' ],    
            ['nombre'=>'Sierra Leone' ],
            ['nombre'=>'Liberia' ],
            ['nombre'=>'Burkina Faso' ],
            ['nombre'=>'Ghana' ],
            ['nombre'=>'Togo' ],
            ['nombre'=>'Benin' ],
            ['nombre'=>'Niger' ],
            ['nombre'=>'Nigeria' ],
            ['nombre'=>'Chad' ],
            ['nombre'=>'Sudan' ],
            ['nombre'=>'Eritrea' ],
            ['nombre'=>'Ethiopia' ],
            ['nombre'=>'Djibouti' ],
            ['nombre'=>'Sudan' ],
            ['nombre'=>'Oman' ],
            ['nombre'=>'Yemen' ],
            ['nombre'=>'Somalia' ],
            ['nombre'=>'Kenya' ],
            ['nombre'=>'Uganda' ],    
            ['nombre'=>'Central African Republic' ],
            ['nombre'=>'Cameroon' ],
            ['nombre'=>'Gabon' ],  
            ['nombre'=>'Angola' ],    
            ['nombre'=>'Tanzania' ],
            ['nombre'=>'Malawi' ],
            ['nombre'=>'Zambia' ],
            ['nombre'=>'Mozambique' ],
            ['nombre'=>'Malawi' ],
            ['nombre'=>'Zambia' ],
            ['nombre'=>'Zimbabwe' ],
            ['nombre'=>'Botswana' ],
            ['nombre'=>'Namibia' ],
            ['nombre'=>'South Africa' ],
            ['nombre'=>'Madagascar' ],    
            ['nombre'=>'New Zealand' ],
            ['nombre'=>'Solomon Islands' ],
            ['nombre'=>'Vanuatu' ],
            ['nombre'=>'New Caledonia' ],
            ['nombre'=>'Fiji'],
            ['nombre'=>'Bahamas']
        ];

        foreach ($paises as $pais) {
            DB::table('pais')->insert($pais);
        }

    }

}

class TipoConvenioTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('tipo_convenio')->delete();

        $tiposConvenio =
        [
            ['id'=>1, 'nombre' => 'MARCO', 'descripcion'=>'tipo contrato Marco'],
            ['id'=>2, 'nombre' => 'INTERINSTITUCIONAL', 'descripcion'=>'tipo contrato Interinstitucional'],
            ['id'=>3, 'nombre' => 'ESPECIFICO', 'descripcion'=>'tipo contrato Especifico']
        ];

        foreach ($tiposConvenio as $tipoConvenio) {
            DB::table('tipo_convenio')->insert($tipoConvenio);
        }

    }

}

class InstitucionTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('institucion')->delete();

        $instituciones =
        [
            ['id'=> 1, 'nombre' => 'CARACTERES SUD S.A.', 'id_pais' => 9 , 'id_tipo_institucion' => 2], // ELIMINAR CUANDO FUNCIONE

        ];

        foreach ($instituciones as $institucion) {
            DB::table('institucion')->insert($institucion);
        }

    }

}

class ConvenioTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('convenio')->delete();

        $convenios =
        [
            ['año' => '2013', 'id_tipo_convenio' => 3, 'id_institucion' => 1  ], // ELIMINAR CUANDO FUNCIONE
        ];

        foreach ($convenios as $convenio) {
            DB::table('convenio')->insert($convenio);
        }

    }

}




