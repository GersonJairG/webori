<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConvenioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convenio', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('año');        
                             
            $table->integer('id_tipo_convenio')->unsigned();
            $table->integer('id_institucion')->unsigned();
            $table->timestamps();			
            
            
            $table->foreign('id_tipo_convenio')->references('id')->on('tipo_convenio')->onDelete('cascade');	
            $table->foreign('id_institucion')->references('id')->on('institucion')->onDelete('cascade');	
		});
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convenio');
    }
}
