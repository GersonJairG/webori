<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsuarioTable extends Migration
{
    /**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuario', function(Blueprint $table)
		{
			$table->string('id',7)->unique();
			$table->string('nombre');
			$table->string('documento',10)->unique();
			$table->string('email');
			$table->string('password');			
			$table->string('sexo');
			$table->integer('id_rol')->unsigned();
			$table->rememberToken();
			$table->timestamps();			
			
			$table->foreign('id_rol')->references('id')->on('rol')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('usuario');
	}
}
