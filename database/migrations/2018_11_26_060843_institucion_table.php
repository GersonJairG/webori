<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstitucionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institucion', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('nombre'); 
            $table->integer('id_pais')->unsigned();              
            $table->integer('id_tipo_institucion')->unsigned();     
            
            $table->timestamps();			
            
            $table->foreign('id_pais')->references('id')->on('pais')->onDelete('cascade');	
            $table->foreign('id_tipo_institucion')->references('id')->on('tipo_institucion')->onDelete('cascade');	
		});
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institucion');
    }
}
